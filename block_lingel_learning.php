<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/completion/classes/cm_completion_details.php');

use core_completion\cm_completion_details;

/**
 * Access for the block block_lingle_learning plugin.
 *
 * @package block_lingle_learning
 * @author Abhishek Karadbhuje <abhishek.karadbhuje@gmail.com>
 */

class block_lingel_learning extends block_base {
    /**
     * Initialises the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_lingel_learning');
    }

    /**
     * This function will prepare course module list.
     * @param int $courseid Course id.
     * @return array $cms course modules.
     * @author Abhishek Karadbhuje <abhishek.karadbhuje@gmail.com>
     */
    public function get_coursemodule_list($courseid) {
        global $USER;

        $modinfo = get_fast_modinfo($courseid);

        $moddata = array();
        foreach ($modinfo->get_cms() as $cm) {
            $cmid = $cm->__get('id');
            $modname = $cm->__get('modname');
            $cminstance = get_coursemodule_from_id($modname, $cmid);
            $completioninfo = cm_completion_details::get_instance($cm, $USER->id);
            $completion = $completioninfo->get_overall_completion();

            $module = new stdClass();
            $module->cmid = $cmid;
            $module->activityname = $cm->get_formatted_name();
            $module->dateadded = date('d-M-Y', $cminstance->added);
            $module->url = $cm->get_url();
            $module->completionstatus = $completion;
            $moddata[] = $module;
        }

        return $moddata;
    }

    /**
     * Gets the block contents.
     *
     * @return string The block HTML.
     */
    public function get_content() {
        global $OUTPUT, $COURSE;

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->footer = '';

        $templatedata = new stdClass();
        $templatedata->cms = $this->get_coursemodule_list($COURSE->id);

        $this->content->text = $OUTPUT->render_from_template('block_lingel_learning/main', $templatedata);

        return $this->content;
    }

    /**
     * Defines in which pages this block can be added.
     *
     * @return array of the pages where the block can be added.
     */
    public function applicable_formats() {
        return [
            'admin' => false,
            'site-index' => false,
            'course-view' => true,
            'mod' => false,
            'my' => false,
        ];
    }

    /**
     * This method can will check the block can be added or not to a page.
     *
     * @param moodle_page $page The page where this block will be added.
     * @return bool Whether the block can be added or not to the given page.
     */
    public function can_block_be_added(moodle_page $page): bool {
        return $page->__get('pagelayout') == 'course';
    }
}
