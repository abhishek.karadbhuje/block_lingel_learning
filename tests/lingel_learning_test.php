<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_lingel_learning;

class lingel_learning_test extends \advanced_testcase {
    public static function setUpBeforeClass(): void {
        global $CFG;

        require_once(__DIR__ . '/../../moodleblock.class.php');
        require_once(__DIR__ . '/../block_lingel_learning.php');
    }

    /**
     * Test the get_course_modulelist method.
     * @covers ::get_course_module_list.
     */
    public function test_course_modulelist(): void {
        $this->resetAfterTest(true);
        $course = $this->getDataGenerator()->create_course();

        $generator = $this->getDataGenerator()->get_plugin_generator('mod_quiz');
        $generator->create_instance(array('course' => $course->id));

        $generator = $this->getDataGenerator()->get_plugin_generator('mod_page');
        $generator->create_instance(array('course' => $course->id));

        $block = new \block_lingel_learning();
        $activitycount = count($block->get_coursemodule_list($course->id));

        $this->assertEquals(2, $activitycount, 'There was a problem counting activity in course');

        $generator = $this->getDataGenerator()->get_plugin_generator('mod_forum');
        $generator->create_instance(array('course' => $course->id));

        $block = new \block_lingel_learning();
        $activitycount = count($block->get_coursemodule_list($course->id));

        $this->assertEquals(3, $activitycount, 'There was a problem counting activity in course');
    }

    /**
     * Test wether the block can be added or not.
     * @covers ::can_block_be_added.
     */
    public function test_can_block_be_added(): void {
        global $PAGE;

        $page = new \moodle_page();
        $block = new \block_lingel_learning();

        $layouts = $PAGE->__get('theme')->layouts;
        foreach (array_keys($layouts) as $layout) {
            $page->set_pagelayout($layout);

            if ($layout == 'course') {
                $this->assertTrue($block->can_block_be_added($page), "There was a problem in layout $layout");
            } else {
                $this->assertFalse($block->can_block_be_added($page), "There was a problem in layout $layout");
            }
        }
    }
}
